<?php

require('setup.php');

$smarty = new Smarty_todo_list();

$tasks = get_tasks($conn);
$lists = get_lists($conn);

$smarty->assign('tasks', $tasks);
$smarty->assign('lists', $lists);

$smarty->display('templates/index.tpl.php');

?>