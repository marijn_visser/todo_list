{include file="header.tpl.php" title='Home Pagina'}

<main class="row">
    <div class="small-12 columns">
        <div class="row">
        <div class="small-12 columns">
            Filteren: <input type="text" id="myInput" onkeyup="filter_task()" placeholder="Zoeken">
        </div>
            {foreach $lists as $list}
                <h2>{$list.name}</h2>
                <a href="/jaar_2/todo_list/modules/update_list.php?id={$list.id}" class="button tiny warning">Edit</a>
                <a href="/jaar_2/todo_list/modules/delete_list.php?id={$list.id}" class="button tiny alert">Delete</a>
                <table id="myTable">
                    <thead>
                        <tr>
                            <th width="200">Naam</th>
                            <th width="400">Beschrijving</th>
                            <th width="200">Tijd</th>
                            <th width="150">Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach $tasks as $task}
                        {if $task.list_id == $list.id}
                            <tr>
                                <td>{$task.task_name}</td>
                                <td>{$task.description}</td>
                                <td>{$task.time}</td>
                                <td>{$task.status}</td>
                                <td>
                                    <a href="/jaar_2/todo_list/modules/update_task.php?id={$task.task_id}" class="button small warning">Edit</a>
                                    <a href="/jaar_2/todo_list/modules/delete_task.php?id={$task.task_id}" class="button small alert">Delete</a>
                                </td>
                            </tr>
                        {/if}
                    {/foreach}                       
                    </tbody>
                </table>
                
            {/foreach}
        </div>
    </div>
             
</main>

{include file="footer.tpl.php"}
