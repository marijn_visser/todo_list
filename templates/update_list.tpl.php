{include file="header.tpl.php" title='Lijst toevoegen'}

<main>
    <div class="row">
        <div class="small-12 columns">          
            <form action="update_list.php?id={$list[0].id}" method="post">
                <input type="hidden" name="id" value="{$list[0].id}">
                <div class="row">
                    <div class="small-6 small-offset-3 columns">
                        <label>Naam:
                            <input name="name" type="text" placeholder="Naam" value="{$list[0].name}">
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="small-6 small-offset-3 columns">
                        <button class="button" name="submit">Bijwerken</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</main>

{include file="footer.tpl.php"}