 {include file="header.tpl.php" title='taak toevoegen'}

<main>
    <div class="row">
        <div class="small-12 columns">          
            <form action="add_task.php" method="post">
                <div class="row">
                    <div class="small-6 small-offset-3 columns">
                        <label>Naam:
                            <input name="name" type="text" placeholder="Naam">
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="small-6 small-offset-3 columns">
                        <label>Beschrijving:
                            <input name="description" type="text" placeholder="Beschrijving">
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="small-6 small-offset-3 columns">
                        <label>Tijd:
                            <input name="time" type="time" placeholder="Tijd">
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="small-6 small-offset-3 columns">
                        <label>Lijst:
                            <select name="list_id">
                                {foreach $lists as $list}
                                    <option value="{$list.id}">{$list.name}</option>
                                {/foreach}
                            </select>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="small-6 small-offset-3 columns">
                        <button class="button" name="submit">Toevoegen</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</main>

{include file="footer.tpl.php"}