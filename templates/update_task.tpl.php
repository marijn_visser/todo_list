{include file="header.tpl.php" title='taak Bijwerken'}
<main>
    <div class="row">
        <div class="small-12 columns">          
            <form action="update_task.php?id={$task[0].id}" method="post">
                <input type="hidden" name="id" value="{$task[0].id}">
                <div class="row">
                    <div class="small-6 small-offset-3 columns">
                        <label>Naam:
                            <input name="name" type="text" placeholder="Naam" value='{$task[0].name}'>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="small-6 small-offset-3 columns">
                        <label>Beschrijving:
                            <input name="description" type="text" placeholder="Beschrijving" value='{$task[0].description}'>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="small-6 small-offset-3 columns">
                        <label>Tijd:
                            <input name="time" type="time" placeholder="Tijd" value='{$task[0].time}'>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="small-6 small-offset-3 columns">
                        <label>Lijst:
                            <select name="list_id">
                                {foreach $lists as $list}
                                    <option value="{$list.id}" {if $task[0].list_id == $list.id}selected{/if}>{$list.name}</option>
                                {/foreach}
                            </select>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="small-6 small-offset-3 columns">
                        <label>Status:
                            <select name="status">
                                <option value="active" {if $task[0].status == 'active'}selected{/if}>Actief</option>
                                <option value="completed" {if $task[0].status == 'completed'}selected{/if}>Voltooid</option>
                            </select>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="small-6 small-offset-3 columns">
                        <button class="button" name="submit">Bijwerken</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</main>

{include file="footer.tpl.php"}