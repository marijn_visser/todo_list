<!doctype html>
<html class="no-js" lang="nl">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>{$title|default:'Homepage'}</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.4.3/dist/css/foundation-float.min.css" integrity="sha256-TPcVVrzfTETpAWQ8HhBHIMT7+DbszMr5n3eFi+UwIl8= sha384-+aXh7XSzITwlvjelsNWuL1A9rT8pWGaiqMMeUjtKcsWIfzT1oV8Mp3oYxmjPK8Gv sha512-cArttU/Yh+PzfQ/dhCdfBiU9+su+fuCwFxLrlLbvuJE/ynUbstaKweVPs7Hdbok9jlv9cwt+xdk20wRz7oYErQ==" crossorigin="anonymous">
    <link rel="stylesheet" href="/jaar_2/todo_list/css/style.css" />

  </head>
  <body>
    <div class="row">
      <div class="small-12 columns menu_wrapper">
          <ul class="menu">
            <li><a href="/jaar_2/todo_list/">Lijst overzicht</a></li>
            <li><a href="/jaar_2/todo_list/modules/add_list.php">Lijst toevoegen</a></li>
            <li><a href="/jaar_2/todo_list/modules/add_task.php">Taak toevoegen</a></li>
          </ul>
      </div>
    </div>
  

    
