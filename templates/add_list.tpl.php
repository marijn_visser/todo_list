{include file="header.tpl.php" title='Lijst toevoegen'}

<main>
    <div class="row">
        <div class="small-12 columns">          
            <form action="add_list.php" method="post">
                <div class="row">
                    <div class="small-6 small-offset-3 columns">
                        <label>Naam:
                            <input name="name" type="text" placeholder="Naam">
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="small-6 small-offset-3 columns">
                        <button class="button" name="submit">Toevoegen</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</main>

{include file="footer.tpl.php"}