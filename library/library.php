<?php

function db_connection(){

    $servername = "localhost";
    $username = "root";
    $password = "root";

    try {
        $conn = new PDO("mysql:host=$servername;dbname=todo_list", $username, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
    }
    catch(PDOException $e){
        echo "Connection failed: " . $e->getMessage();
    }
    return $conn;
}
// Make database connection
$conn = db_connection();

function d($debug, $highlight = true, $hidden = false){
    // Highlight debug string
    if($highlight){
        $debug = highlight_string('<?php '.print_r($debug, true).' ?>', true);
        $debug = str_replace(array('&lt;?php&nbsp;','?&gt;'), '', $debug);
    }
    else {
        $debug = print_r($debug, true);
    }

    // Check if debug is hidden
    print($hidden ? "<!--\r\n" : "");

    // Print input
    print("<pre style='text-align: left;'>\r\n");
    print($debug);
    print("</pre>\r\n");

    // Seperate debug call
    print("<hr />\r\n");

    // Check if debug is hidden
    print($hidden ? "-->\r\n" : "");

    // Flush input
    flush();
}

function add_list($conn, $value){

    $sql = 'INSERT INTO
                lists(
                    name)
                VALUES( 
                    :name)';                
    $stm = $conn->prepare($sql);
    $stm->bindParam(':name', $value['name']);
    $stm->execute();

}

function add_task($conn, $value){

    $sql = 'INSERT INTO
                tasks(
                    list_id,
                    name,
                    description,
                    time,
                    status)
                VALUES(
                    :list_id,
                    :name,
                    :description,
                    :time,
                    "active")';                
    $stm = $conn->prepare($sql);
    $stm->bindParam(':list_id',     $value['list_id']);
    $stm->bindParam(':name',        $value['name']);
    $stm->bindParam(':description', $value['description']);
    $stm->bindParam(':time',        $value['time']);
    $stm->execute();

}

function get_lists($conn){

    $sql = $conn->prepare("
        SELECT
            *
        FROM
            lists
    ");
    $sql->execute();
    $result = $sql->fetchAll();
  
    return $result;
}

function get_tasks($conn){

    $sql = $conn->prepare("
        SELECT
            tasks.id as task_id,
            tasks.list_id,
            tasks.name AS task_name,
            tasks.description,
            tasks.time,
            tasks.status,
            lists.id,
            lists.name AS list_name
        FROM
            tasks,
            lists
        WHERE 1=1
            AND tasks.list_id = lists.id
    ");
    $sql->execute();
    $result = $sql->fetchAll();
  
    return $result;
}

function get_list_on_id($conn, $value){

    $sql = $conn->prepare("
        SELECT
            *
        FROM
            lists
        WHERE
            id = ".$value['id']."
    ");
    $sql->execute();
    $result = $sql->fetchAll();
  
    return $result;
}

function get_task_on_id($conn, $value){

    $sql = $conn->prepare("
        SELECT
            tasks.id,
            tasks.list_id,
            tasks.name,
            tasks.description,
            tasks.time,
            tasks.status,
            lists.name AS list_name            
        FROM
            tasks,
            lists
        WHERE 1=1
           AND tasks.id = ".$value['id']."
           AND tasks.list_id = lists.id
    ");
 
    $sql->execute();
    $result = $sql->fetchAll();
  
    return $result;
}

function delete_list($conn, $value){

    $sql['delete_list'] = "DELETE FROM
                            lists
                        WHERE
                            id = ".$value['id']."
                          ";

    $sql['delete_tasks'] = "DELETE FROM
                                tasks
                            WHERE
                                list_id = ".$value['id']."
                            ";      
    $stm = $conn->prepare($sql['delete_list']);
    $stm->execute();

    $stm = $conn->prepare($sql['delete_tasks']);
    $stm->execute();

}

function delete_task($conn, $value){

    $sql = "DELETE FROM
                tasks 
            WHERE
                id = ".$value['id']."
            ";                                      
    $stm = $conn->prepare($sql);
    $stm->execute();

}

function update_list($conn, $value){

     $sql = "UPDATE
                lists
            SET
                name = '".$value['name']."'
            WHERE
                id = ".$value['id']."
            ";

    $stm = $conn->prepare($sql);
    $stm->execute();

}

function update_task($conn, $value){

    $sql = "UPDATE
               tasks
           SET
               list_id     = '".$value['list_id']."',
               name        = '".$value['name']."',
               description = '".$value['description']."',
               time        = '".$value['time']."',
               status      = '".$value['status']."'                           
           WHERE
               id = ".$value['id']."
           ";
                                                        
   $stm = $conn->prepare($sql);
   $stm->execute();

}

?>