<?php

$page_title = "Lijst Toevoegen";

require('../setup.php');

$smarty = new Smarty_todo_list();

if ($_POST) {

    add_list($conn, $_POST);

    header('Location: /jaar_2/todo_list/');
    exit;
}

$smarty->display('add_list.tpl.php');

?> 