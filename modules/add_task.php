<?php

require('../setup.php');

$smarty = new Smarty_todo_list();

$lists = get_lists($conn);

$smarty->assign('lists', $lists);

if ($_POST) {

    add_task($conn, $_POST);

    header('Location: /jaar_2/todo_list/');
    exit;
}    

$smarty->display('add_task.tpl.php');

?> 