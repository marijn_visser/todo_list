<?php

require('../setup.php');

$smarty = new Smarty_todo_list();

$result = get_task_on_id($conn, $_GET);

$smarty->assign('task', $result);

if (!empty($_POST) || !isset($_POST)) {

    delete_task($conn, $_POST);
    header('Location: /jaar_2/todo_list/');
    exit;
}

$smarty->display('delete_task.tpl.php');


?>