<?php

$page_title = "Lijst Bijwerken";

require('../setup.php');

$smarty = new Smarty_todo_list();

$result = get_list_on_id($conn, $_GET);

$smarty->assign('list', $result);

if ($_POST) {

    update_list($conn, $_POST);

    header('Location: /jaar_2/todo_list/');
    exit;
}

$smarty->display('update_list.tpl.php');

?> 