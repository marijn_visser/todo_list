<?php

require('../setup.php');

$smarty = new Smarty_todo_list();

$lists = get_lists($conn);
$result = get_task_on_id($conn, $_GET);

$smarty->assign('lists', $lists);
$smarty->assign('task', $result);

if ($_POST) {

    update_task($conn, $_POST);
    
    header('Location: /jaar_2/todo_list/');
    exit;
}

$smarty->display('update_task.tpl.php');

?> 